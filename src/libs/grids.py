# NASA IDS Project
#
# Copyright (c) 2019 Wu Sun <wsun@carnegiescience.edu>
# Michalak Lab
# Department of Global Ecology
# Carnegie Institution for Science
"""Grid configuration and functions."""
from typing import List, Tuple

import numpy as np
import pandas as pd

from config import dirs


def _read_NA_coords() -> pd.DataFrame:
    """Read coordinates of the North American grid."""
    path: str = f"{dirs.data}/coords/na-grid.csv"
    coords: pd.DataFrame = pd.read_csv(path, encoding="utf-8", engine="c")
    return coords


N_NA_GRID: int = 2613
_coords: pd.DataFrame = _read_NA_coords()


def get_NA_grid_index(
    edge_grid: bool = False,
) -> Tuple[np.ndarray, np.ndarray, Tuple[np.ndarray, np.ndarray]]:
    """Get coords and index of land cells in a 2-D North American grid."""
    coords_arr: np.ndarray = _coords.values
    lats: np.ndarray = coords_arr[:, 0].reshape(-1, 1)  # column vector
    lons: np.ndarray = coords_arr[:, 1].reshape(-1, 1)  # column vector
    # generate grid and identify indices
    min_lat, max_lat = lats.min(), lats.max()
    min_lon, max_lon = lons.min(), lons.max()
    lon_grid, lat_grid = np.meshgrid(
        np.arange(min_lon, max_lon + 0.5), np.arange(min_lat, max_lat + 0.5)
    )
    index_i: List[int] = []
    index_j: List[int] = []
    # match grid point by point
    for lon, lat in zip(lons, lats):
        i_arr, j_arr = np.where((lon_grid == lon) & (lat_grid == lat))
        index_i.append(i_arr.item())
        index_j.append(j_arr.item())
    # create grid index
    grid_index: Tuple[np.ndarray, np.ndarray] = (
        np.array(index_i),
        np.array(index_j),
    )
    # if edge_grid is set True, return edges of grid cells (for pcolormesh)
    if edge_grid:
        lon_grid_edges, lat_grid_edges = np.meshgrid(
            np.arange(min_lon - 0.5, max_lon + 1.0),
            np.arange(min_lat - 0.5, max_lat + 1.0),
        )
        return lon_grid_edges, lat_grid_edges, grid_index
    else:
        return lon_grid, lat_grid, grid_index


def get_NA_grid_coords() -> Tuple[np.ndarray, np.ndarray]:
    """Get the lon-lat grid coordinates of the North American land domain."""
    lon_grid, lat_grid, grid_index = get_NA_grid_index(edge_grid=False)
    return lon_grid[grid_index], lat_grid[grid_index]
