# NASA IDS Project
#
# Copyright (c) 2022 Wu Sun <wsun@carnegiescience.edu>
# Michalak Lab
# Department of Global Ecology
# Carnegie Institution for Science
"""Directory settings."""
import os

# project root directory
root: str = os.path.abspath(os.path.join(os.path.dirname(__file__), "../.."))

# source files
src: str = f"{root}/src"

# plots
plots: str = f"{root}/plots"

# data
data: str = f"{root}/data"
