# Data and code for analysis of biome-scale temperature sensitivities of ecosystem respiration

[![doi](https://zenodo.org/badge/doi/10.5281/zenodo.7874439.svg)](https://doi.org/10.5281/zenodo.7874439)

Wu Sun (wsun@carnegiescience.edu)

Department of Global Ecology, Carnegie Institution for Science

Last updated on 2023-04-28

This repository presents data and code used to produce results in the following
work:

Sun, W., Luo, X., Fang, Y., Shiga, Y. P., Zhang, Y., Fisher, J. B., Keenan, T.
F., and Michalak, A. M. (2023). Biome-scale temperature sensitivity of
ecosystem respiration revealed by atmospheric CO<sub>2</sub> observations.
_Nature Ecology & Evolution_, in press.

<details>
  <summary>BibTeX citation</summary>

```bibtex
@article{sun_et_al_2023,
  author   = {Wu Sun and Xiangzhong Luo and Yuanyuan Fang and Yoichi P. Shiga and Yao Zhang and Joshua B. Fisher and Trevor F. Keenan and Anna M. Michalak},
  title    = {Biome-scale temperature sensitivity of ecosystem respiration revealed by atmospheric {CO\textsubscript{2}} observations},
  year     = {2023},
  journal  = {Nature Ecology \& Evolution},
  note     = {In press}
}
```
</details>

## Data

The following data files are provided in the `data` folder for reproducing the
results of this study.

* `coords/na-grid.csv`: Coordinates of the North American land grid at 1° × 1°
  resolution.
* `biomes.nc`: North American biome map at 1° × 1° resolution.
* `sites.csv`: List of sites where atmospheric CO<sub>2</sub> concentrations
  were monitored.
* `seasonal-cycles.nc`: Biome-integrated seasonal cycles of carbon flux
  estimates from models.
* `seasonal-cycles-corrected.nc`: Biome-integrated seasonal cycles of carbon
  flux estimates from models, with ecosystem respiration estimates adjusted
  using the ensemble-optimal temperature sensitivity for North America.
* `H-monthly-summed.nc`: Monthly summed transport footprints that link point
  measurements of concentrations to gridded fluxes.
* `global-and-na-resp.csv`: Estimates of global and North American total
  ecosystem respiration (units: PgC yr<sup>–1</sup>).
* `temperature-sensitivity.csv`: Temperature sensitivities of ecosystem
  respiration diagnosed from model outputs.
* `temperature-sensitivity-sm.csv`: Temperature sensitivities of ecosystem
  respiration diagnosed from model outputs, with soil moisture influences
  removed.
* `temperature-sensitivity-rad.csv`: Temperature sensitivities of ecosystem
  respiration diagnosed from model outputs, with the partial influence of
  radiation removed.
* `temperature-sensitivity-acc.csv`: Temperature sensitivities of ecosystem
  respiration diagnosed from model outputs, with thermal acclimation effects
  accounted for.
* `temperature-sensitivity-corrected.csv`: Optimized temperature sensitivities
  of ecosystem respiration for the models.
* `temperature-sensitivity-corrected-comb.csv`: Optimized temperature
  sensitivities of ecosystem respiration in GPP combination tests.
* `temperature-sensitivity-corrected-lat.csv`: Optimized temperature
  sensitivities of ecosystem respiration based on a test of the impact of the
  lateral carbon fluxes.
* `e-act-opt.csv`: Ensemble-optimal estimates of the temperature sensitivity of
  ecosystem respiration.
* `explanatory-power.csv`: Explanatory power of original carbon flux estimates
  from models.
* `explanatory-power-resp-corrected.csv`: Explanatory power of carbon flux
  estimates, with ecosystem respiration estimates adjusted using the
  ensemble-optimal temperature sensitivity for North America.
* `explanatory-power-resp-corrected-by-model.csv`: Explanatory power of carbon
  flux estimates, with ecosystem respiration estimates adjusted using the
  optimized temperature sensitivity for each model.

## Code

Python code for reproducing the figures is in the `src` folder. Before running
the code, please ensure that you have the following prerequisites installed.

* Python >= 3.8 (Python 2.x not supported)
* Jupyter Notebook
* numpy
* scipy
* pandas
* xarray
* statsmodels
* matplotlib
* seaborn
* cartopy

The codebase is organized as follows:

* `config`: Configuration.
* `libs`: Custom functions for data processing.
* `make_plots.ipynb`: Analyze model estimates of temperature sensitivity of
  ecosystem respiration and make figures.

## License

This repository is licensed under either the [MIT License](./LICENSE) or the
[CC BY 4.0 License](./LICENSE-CC-BY). You may choose the license that best
suits your intended use case. By accepting the license, you are granted
permission to use this repository without requiring additional written consent
from the authors.

`SPDX-License-Identifier: CC-BY-4.0 OR MIT`

Please contact Wu Sun (wsun@carnegiescience.edu) if you have any questions.
